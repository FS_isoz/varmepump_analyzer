import numpy as np
import pylab as pyl
import pandas as pd
import os
import path
import regex as re
import datetime


class dataReader():
    def __init__(self, file_path: str):
        self.base_path = file_path
        self.file_regex = re.compile(r"(?P<file_id>\d{1,2}).txt")
        self.column_names = {
            "1": "data1",
            "2": "data2",
            "3": "data3",
            "4": "data4",
            "5": "data5",
            "6": "data6",
            "7": "data7",
            "8": "data8",
            "9": "data8",
            "10": "data10",
            "13": "data13",
            "14": "data14",
            "15": "data15",
            "16": "data16",
            "17": "data17",
            "18": "data18"
        }
        self.data_list = []
        self.dataframe = None

    def read_files(self):
        dir_list = os.listdir(self.base_path)
        # FIXME extract day and month from path
        for file_name in dir_list:
            file_match = self.file_regex.match(file_name)
            day_str = "1"
            month_str = "1"
            year_str = "2020"

            if file_match:
                print(file_match['file_id'])
                fid = open(os.path.join(self.base_path,file_name))
                file_data = np.genfromtxt(os.path.join(self.base_path, file_name),delimiter=",",
                                                    dtype=[("time",np.object),(self.column_names[file_match['file_id']],np.float)],
                                          autostrip=True,
                                          converters={ 
                                              0: lambda x:datetime.datetime.strptime(f'{year_str}{month_str}{day_str} {x.decode("ASCII")}', "%Y%m%d %H:%M:%S"),
                                              1: np.float 
                                          } 
                                          ) 
                self.data_list.append(file_data)

    def join_files(self):
        self.dataframe =  pd.DataFrame(self.data_list[0])
        for data_arr in self.data_list[1:]:
            column_name = data_arr.dtype.names[1]
            if len(self.dataframe['time']) == len(data_arr['time'])\
                and (self.dataframe['time'] == data_arr['time']).all():
                    self.dataframe[column_name] = data_arr[column_name]
            else:
                print(f"file for column {column_name} are missing data points")
        print("done")

    def save_file(self,fname_out:str=None):
        if not fname_out:
            print("save_file - ERROR No filename is given ")
            raise NameError
        self.dataframe.to_csv(fname_out)

    def plot_data(self):
        for data_key in self.column_names:
            if self.column_names[data_key] in self.dataframe:
                pyl.plot(self.dataframe['time'],self.dataframe[self.column_names[data_key]],label=self.column_names[data_key])
        pyl.legend()
        pyl.show()
            
            
        

if __name__ == "__main__":
    dp = dataReader(file_path=r'C:\Eget\Pappa_PyScr\testData\01\31')
    dp.read_files()
    dp.join_files()
    dp.plot_data()
